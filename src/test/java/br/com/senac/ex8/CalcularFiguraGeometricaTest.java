package br.com.senac.ex8;

import static org.junit.Assert.*;
import org.junit.Test;

public class CalcularFiguraGeometricaTest {

    @Test
    public void deveCalcularAreaQuadrago() {
        FiguraGeometrica fg = new Quadrado(10);
        assertEquals(10 * 10, fg.getAreaTotal(), 0.1);

    }

    @Test
    public void deveCalcularAreaRetangulo() {
        FiguraGeometrica fg = new Retangulo(5, 8);
        assertEquals(5 * 8, fg.getAreaTotal(), 0.1);

    }

    @Test
    public void deveCalcularAreaTriangulo() {
        FiguraGeometrica fg = new Triangulo(10, 10);
        assertEquals((10 * 10) / 2, fg.getAreaTotal(), 0.1);

    }

    @Test
    public void deveCalcularAreaCirculo() {
        FiguraGeometrica fg = new Circulo(5);
        assertEquals(3.141592 * (5 * 5), fg.getAreaTotal(), 0.1);

    }
}
