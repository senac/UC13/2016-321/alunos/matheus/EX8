package br.com.senac.ex8;

public class Quadrado extends FiguraGeometrica {

    private double lado;

    public Quadrado() {
    }

    public Quadrado(double lado) {
        this.lado = lado;
    }

    public double getLado() {
        return lado;
    }

    public void setLado(double lado) {
        this.lado = lado;
    }

    @Override
    public double getAreaTotal() {
        return this.lado * this.lado;
    }

}
