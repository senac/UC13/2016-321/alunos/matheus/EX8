package br.com.senac.ex8;

public abstract class FiguraGeometrica {

    public abstract double getAreaTotal();
}
