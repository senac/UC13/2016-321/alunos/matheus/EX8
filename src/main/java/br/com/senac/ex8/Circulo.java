package br.com.senac.ex8;

public class Circulo extends FiguraGeometrica {

    private double raio;
    private double Pi = 3.141592;

    public Circulo() {
    }

    public Circulo(double raio) {
        this.raio = raio;
    }

    public double getRaio() {
        return raio;
    }

    public void setRaio(double raio) {
        this.raio = raio;
    }

    public double getPi() {
        return Pi;
    }

    @Override
    public double getAreaTotal() {
        return this.Pi * (this.raio * this.raio);
    }

}
