package br.com.senac.ex8;

public class Retangulo extends FiguraGeometrica {

    private double altura;
    private double comprimento;

    public Retangulo() {
    }

    public Retangulo(double altura, double comprimento) {
        this.altura = altura;
        this.comprimento = comprimento;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public double getComprimento() {
        return comprimento;
    }

    public void setComprimento(double comprimento) {
        this.comprimento = comprimento;
    }

    @Override
    public double getAreaTotal() {
        return this.altura * this.comprimento;
    }

}
